<?php

use App\Http\Controllers\JugadorController;
use App\Http\Controllers\TorneoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/torneos' , [TorneoController::class , 'index']);
Route::post('/torneos' , [TorneoController::class , 'store']);

Route::get('/jugadores' , [JugadorController::class , 'index']);
Route::post('/jugadores' , [JugadorController::class , 'store']);
