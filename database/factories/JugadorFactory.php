<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class JugadorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $nombre = $this->faker->unique->name(20);
        $numero = $this->faker->numberBetween(0,100);
        return [
            'nombre' => $nombre,
            'nivel_habilidad' => $numero,
            'fuerza' => $numero,
            'vel_desplazamiento' => $numero,
            'tiempo_reaccion' => $numero
        ];
    }
}
